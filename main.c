#include <stdio.h>
#include <stdlib.h>

#include "udp/udp.h"
#include "serial/serial.h"

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        fprintf(stderr, "Usage: udp_to_serial udp_port /dev/ttyUSB#\n");
        return -1;
    }

    serial_ctx_t serial_ctx;
    socket_ctx_t receiver_ctx;

    //setup the serial socket context
    serial_init(&serial_ctx, argv[2]);
    //setup the UDP socket context
    receiver_init(&receiver_ctx, (int)strtol(argv[1], NULL, 10));

    char buffer[UDP_PACKET_SIZE];

    char yaw_string[7];
    char pitch_string[7];
    int yaw;
    int pitch;

    while(1)
    {
        //receive orientation data off of the UDP socket
        receiver_recv(&receiver_ctx, buffer);

        //get the pitch and yaw stirngs
        fprintf(stdout, "received: %s\n", buffer);
        snprintf(yaw_string, 7, "%s", buffer + 1);
        printf("yaw_string %s, pitch_string %s\n", yaw_string, pitch_string);
        snprintf(pitch_string, 7, "%s", buffer + 7);

        //convert the strings to integers
        yaw = strtol(yaw_string, NULL, 10);
        pitch = strtol(pitch_string, NULL, 10);

        printf("yaw: %d, pitch %d\n", yaw, pitch);

        //send the first 3 bytes of each of the pitch and yaw integers
        snprintf(buffer, 9, "[%c%c%c%c%c%c]", 
                 *((unsigned char *)&yaw), *(((unsigned char *)&yaw) + 1), *(((unsigned char *)&yaw) + 2),
                 *((unsigned char *)&pitch), *(((unsigned char *)&pitch) + 1), *(((unsigned char *)&pitch) + 2));
        serial_write(&serial_ctx, buffer, 8);

        printf("wrote to serial %02x%02x%02x %02x%02x%02x\n", 
               *(((unsigned char *)buffer) + 1), *(((unsigned char *)buffer) + 2), *(((unsigned char *)buffer) + 3),
               *(((unsigned char *)buffer) + 4), *(((unsigned char *)buffer) + 5), *(((unsigned char *)buffer) + 6));
    }
    
    receiver_destroy(&receiver_ctx);
    return 0;
}
